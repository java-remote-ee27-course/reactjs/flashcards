import FlashCards from "./solution1.js";
import FlashCards2 from "./solution2.js";

import "./index.css";
function App() {
  return (
    <div className="App">
      <FlashCards />
      <div className="spacer"></div>
      <FlashCards2 />
    </div>
  );
}

export default App;
